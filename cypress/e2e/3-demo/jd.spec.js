describe('Demo for cypress',function(){
    const bookName = '设计模式';
    before(()=>{
        cy.log('I will run before all');

    })
    beforeEach(()=>{
        cy.log('I will run before each');
    })

    it('I want to search the book named "设计模式"',function(){
        cy.visit('http://www.jd.com')
        // 输入书名《设计模式》
        cy.get('#key').type(bookName).should('have.value',bookName)
        cy.screenshot('./screenshots/jdSearchResult');

        // 搜索书本《设计模式》
        cy.get('div.form button.button').click()
        cy.get('div.crumbs-nav-item strong.search-key').should('have.text',"\"" + bookName + "\"")

        // 添加筛选条件：机械工业出版社
        cy.get('#brand-650337').click()
        cy.get('a.crumb-select-item').should('have.text', '品牌：机械工业出版社（CMP）')
        
        // 检查商品价格，名字，出版社
        cy.get('li[data-sku=10026488711271] div.p-price').should('contain', '39.50')
        cy.get('li[data-sku=10026488711271] div.p-name').should('contain', bookName).and('contain', '机械工业出版社')

        // 打开商品详情页
        cy.get('li[data-sku=10026488711271] div.p-img').click()
    })

    afterEach(()=>{
        cy.log('I will running after each')
    })
    after(()=>{
        cy.log('I will run after all');
    })

    Cypress.on('uncaught:exception', (err, runnable) => {
        return false
      })  
})