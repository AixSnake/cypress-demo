# cypress-demo

## Cypress 工程搭建步骤

1. 在gitee上创建git 仓库
2. 克隆仓库到本地
   ```
   git clone https://gitee.com/zhaoruguo/cypress-demo.git
   ```
3. 初始化js工程  
    ``` 
    npm init
    ```
4. 安装cypress 
   ```
    npm install cypress --save-dev
    ```
5. 根据需要配置.gitignore文件
   ```
   node_modules
   cypress/videos/
   ...
   ```
6. 提交代码到远程仓库
    ```
    git add filename   
    git commit -m '描述commit的内容原因等'
    git push
    ```


## 使用说明

使用如下命令打开cypress, 手动选择需要跑的用例
  ```
  npm run cypress:open
  ```
或
  ```
  npx cypress open
  ```

使用如下命令在无头模式下运行 cypress
```
  npm run cypress:run
  ```
或
  ```
  npx cypress run
  ```


如果想要生成测试报告
首先安装 mochawesome
```
 npm install mochawesome
```
执行如下命令生成报告
```
cypress run --reporter mochawesome
```
